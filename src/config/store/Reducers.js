import { combineReducers } from 'redux';
import reduceCheckPoint from '../../service/checkPoint/checkPointReducers';

const rootReducer = combineReducers({
    reduceCheckPoint
  })
  
export default rootReducer;