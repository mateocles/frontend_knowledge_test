import { fork, all } from 'redux-saga/effects';
import checkPointSaga from '../../service/checkPoint/checkPointSagas';

export default function* rootSaga() {
	yield all([
		fork(checkPointSaga),
	]);
}