import createSagaMiddleware from 'redux-saga';
import { createStore, applyMiddleware } from 'redux';
import rootSaga from './Sagas';
import rootReducers from './Reducers';

const sagaMiddleware = createSagaMiddleware();
let middleware = [sagaMiddleware]

//let middleware = [sagaMiddleware, routeMiddleware]
//  if (!env_production) {
//    middleware = [...middleware, logger]
//}

const store = createStore(
  rootReducers,
  applyMiddleware(...middleware)
);
sagaMiddleware.run(rootSaga);
export default store;