import { API_KEY } from '../utils/config'

export class Api {
	get(url, params) {
		url = new URL(`${url}`)
		if (params) Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))
		return fetch(url, {
			method: 'GET'
		})
			.then(async res => {
				const payload = await res.json()
				return payload;
			})
			.catch(err => err)
	}
}
export default new Api()