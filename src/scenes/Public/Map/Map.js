import React, { useEffect } from 'react'
import { check as checkActions } from '../../../service/checkPoint/checkPointActions'
import { useDispatch } from 'react-redux'
import Container from 'react-bootstrap/Container'
import WrappedMap from './Component/MapGoogle'

const Map = () => {

	const dispatch = useDispatch()
	const { get } = checkActions
	useEffect(() => {
		dispatch(get())
	}, [])

	return (
		<Container>
			<div>
				<WrappedMap/>
			</div>
		</Container>
	)
}
export default Map 