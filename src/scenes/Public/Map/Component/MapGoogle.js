import React from 'react'
import {  GoogleMap,  useLoadScript,  Marker,  InfoWindow} from '@react-google-maps/api'
import { formatRelative } from 'date-fns'
import { API_KEY } from '../../../../config/utils/config'
import '@reach/combobox/styles.css'
import mapStyles from './mapStyles'
import Button from 'react-bootstrap/Button'

const libraries = ['places']
const mapContainerStyle = {
  height: "80vh",
  width: "80vw",
}
const options = {
  styles: mapStyles,
  disableDefaultUI: true,
  zoomControl: true,
}

const center = {
  lat: 1.6141841,
  lng: -75.6074759,
}

export default function Mapgoogle() {

  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: API_KEY,
    libraries,
  })
  const [markers, setMarkers] = React.useState([]);
  const [selected, setSelected] = React.useState(null);

  const onMapClick = React.useCallback((e) => {
    setMarkers((current) => [
      ...current,
      {
        lat: e.latLng.lat(),
        lng: e.latLng.lng(),
        time: new Date(),
      },
    ])
  }, [])

  const mapRef = React.useRef();
  const onMapLoad = React.useCallback((map) => {
    mapRef.current = map;
  }, [])

  if (loadError) return "Error";
  if (!isLoaded) return "Loading..."

  return (
    <div>
      <div>
      <h1>
        Sitios de interés
        <span role="img" aria-label="tent">
		🏛️
        </span>
      </h1>
      </div>      
      <Button>Iniciar Ruta</Button>
      <div>
        
      </div>

      <GoogleMap
        id="map"
        mapContainerStyle={mapContainerStyle}
        zoom={15}
        center={center}
        options={options}
        onClick={onMapClick}
        onLoad={onMapLoad}
      >
        {markers.map((marker) => (
          <Marker
            key={`${marker.lat}-${marker.lng}`}
            position={{ lat: marker.lat, lng: marker.lng }}
            onClick={() => {
              setSelected(marker);
            }}
            icon={{
              url: `/flag.svg`,
              origin: new window.google.maps.Point(0, 0),
              anchor: new window.google.maps.Point(15, 15),
              scaledSize: new window.google.maps.Size(30, 30),
            }}
          />
        ))}

        {selected && (
          <InfoWindow
            position={{ lat: selected.lat, lng: selected.lng }}
            onCloseClick={() => {
              setSelected(null);
            }}
          >
            <div>
              <h2>
                <span role="img" > 🚩
                </span>
                Ubicación Seleccionada
              </h2>
              <p>Seleccion {formatRelative(selected.time, new Date())}</p>
            </div>
          </InfoWindow>
        )}
      </GoogleMap>
    </div>
  );
}
