import * as React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import  Map  from './Map/Map';

class Public extends React.Component {
	render() {
		return (
			<Router>
				<Switch className='h-100'>
					<Route exact path='/' component={Map} />
				</Switch>
			</Router>
		);
	}
}
export default Public;