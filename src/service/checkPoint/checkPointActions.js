import {createActions} from 'redux-actions';

export const {check}= createActions({
    CHECK:{
        GET:()=>({}),
        GET_RESPONSE:jsoncheck=>({jsoncheck}),
    }
});