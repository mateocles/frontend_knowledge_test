import { handleActions } from 'redux-actions';

export const INITIAL_STATE = {
  jsoncheck: [],
  loading: false,
  error: false,
  message: undefined
};
const reducer = handleActions({
    CHECK: {
    GET: (state) => ({ ...state, loading: false, error: false }),
    GET_RESPONSE: {
      next(state, { payload: {  jsoncheck } }) {
        return { ...state,  jsoncheck };
      },
      throw(state, { error, payload: { message } }) {
        return { ...state, error, message };
      }
    }
  },
},
INITIAL_STATE
);
export default reducer;