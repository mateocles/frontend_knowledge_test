import { put, takeLatest, all } from 'redux-saga/effects';
import Api from '../../config/common/Api';
import { check } from './checkPointActions';

function* getCheck() {
  const response = yield Api.get('https://restcountries.eu/rest/v2/all');
  if (response.success) {
    yield put(check.getResponse(response.payload));
  } else {
    const err = new TypeError(response.error);
    yield put(check.getResponse(err));
  }
}

function* ActionWatcher() {
  yield takeLatest(check.get, getCheck);
}

export default function* rootSaga() {
  yield all([ActionWatcher()]);
}