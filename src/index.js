
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import App from './scenes/App.js';
import store from '../src/config/store/Index'
import { BrowserRouter } from 'react-router-dom';

render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);